<?php
if (!session_id()) session_start();
if (!$_SESSION['logon']){ 
    header("Location:index.php");
    die();
}
?>
<?php
include 'nav.php';
?>
<html>
<head>


<!-- AJAX start. -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
	<script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#myform").validate({
			debug: false,
			rules: {
				name: "required",
				amount: {
					required: false,
					email: false
				}
			},
			messages: {
				name: "Please let us know who you are.",
				email: "A valid email will help us get in touch with you.", 
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$.post('process.php', $("#myform").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});
	</script>
	<!-- <style>
	label.error { width: 250px; display: inline; color: red;}
	</style>-->
	
<style type="text/css">
body{
margin-top: 150px;
font-family:"Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif;
font-size:12px;
}



p, h1, form, button{border:0; margin:0; padding:0;}
.spacer{clear:both; height:1px;}
/* ----------- My Form ----------- */
.myform{
margin:0 auto;
width:400px;
padding:14px;
}

/* ----------- stylized ----------- */
#stylized{
border:solid 2px #b7ddf2;
background:#ebf4fb;
}
#stylized h1 {
font-size:14px;
font-weight:bold;
margin-bottom:8px;
}
#stylized p{
font-size:11px;
color:#666666;
margin-bottom:20px;
border-bottom:solid 1px #b7ddf2;
padding-bottom:10px;
}
#stylized label{
display:block;
font-weight:bold;
text-align:right;
width:140px;
float:left;
}
#stylized .small{
color:#666666;
display:block;
font-size:11px;
font-weight:normal;
text-align:right;
width:140px;
}
#stylized input{
float:left;
font-size:12px;
padding:4px 2px;
border:solid 1px #aacfe4;
width:200px;
margin:2px 0 20px 10px;
}

#stylized button{
clear:both;
margin-left:150px;
width:92px;
height:31px;
background:#666666 url(img/button.png) no-repeat;
text-align:center;
line-height:31px;
color:#FFFFFF;
font-size:11px;
font-weight:bold;
}
</style>
	
</head>

<body>
<p><center><img src="images/message.png"></img> 
<?php
include 'connect.php';
$query=mysql_query("SELECT * FROM general");
	$numrows=mysql_num_rows($query);
	if($numrows!=0)
	{
		// fetch all rows that are available into variable $row 
		while($row=mysql_fetch_assoc($query))
		{
			$dbattnmsg=$row['attnmsg'];
			$dbmonth=$row['month'];
			}
			$count=strlen($dbattnmsg);
			if($count>=1){
			echo("<font color='blue'>".$dbattnmsg."</font>");
			}
			else{
			echo("<font color='blue'> No new messages! </font>");
			}
			} else {
			echo("<font color='blue'> No new messages! </font>");
			}
			
			?></center><br>
<div id="stylized" class="myform">
<form name="myform" id="myform" action="" method="POST"> 
<h1>Add New Purchase</h1>
<p>Please fill in all details</p>
<div id="results"></div>

<label>Store
<span class="small">Walmart, Lammers etc.</span>
</label>
<input type="text" name="merchant" id="merchant" />

<label>Date
<span class="small">Date on your receipt</span>
</label>
<input type="text" name="date" id="date" value="<?php echo (date("m.d.y"));?>" />

<label>Amount
<span class="small">Amount you spent</span>
</label>
<input type="text" name="amount" id="amount" value="<?php echo ("0.00");?>" />

<label>Remarks
<span class="small">What did you buy?</span>
</label>
<input type="text" name="remarks" id="remarks" />

<div class="spacer"></div>

<!-- The Submit button -->
	<button type="submit">Add Purchase</button>
</form>
<!-- We will output the results from process.php here -->
<div id="results"><div>
 <div class="spacer"></div> 
 </div>

 <!-- the following code is to check if the session username is admin or not -->
  <?php
  include 'connect.php';
// after the database is connected and the right database has been selected
$username=$_SESSION['first_name'];
	$query=mysql_query(" SELECT * FROM users WHERE username='$username' ");
	$numrows=mysql_num_rows($query);
	if($numrows!=0)
	{
		// fetch all rows that are available into variable $row and extract username and password from the database
		while($row=mysql_fetch_assoc($query))
		{
			$dbusername=$row['username'];
			$dblevel=$row['level'];
			}
				
			if($username==$dbusername && $dblevel=='admin'){
				echo ("<p><form action='admin.php' method='POST'><button style='background: url(images/admin.gif); color: white; width:92; height:22; background-repeat:no-repeat;' type='submit'></button><input type='hidden' name='admin' value='admin'></input></form>");
				
				
				}
				else echo "";
	}
	else echo ("no record found");
				
			
	
	?>
				
</div>



</html>